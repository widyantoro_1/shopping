package com.example.demo.service;

import com.example.demo.domain.Shopping;
import com.example.demo.domain.Users;
import com.example.demo.repo.ShoppingRepo;
import com.example.demo.repo.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import java.util.List;

@Service
public class ShoppingService {
    @Autowired
    ShoppingRepo shoppingRepo;

    @Autowired
    UsersRepo usersRepo;

    public void save(Users users){
        usersRepo.save(users);
    }

    public void saveShopping(Shopping shopping){
        shoppingRepo.save(shopping);
    }
    public void update(Shopping shopping, Integer idShopping){
        shopping.setId(idShopping);
        shoppingRepo.save(shopping);
    }
    public List<Users> findAllUsers(){
        return usersRepo.findAll();
    }
    public Shopping findByIdShopping(Integer idShopping){
        return shoppingRepo.findAllById(idShopping);
    }
    public void delete(Integer idShopping){
        shoppingRepo.deleteById(idShopping);
    }
    public List<Shopping> ShoppingAll(){
        return shoppingRepo.findAllShopping();
    }
    public Users findUsersByNamaAndEmail(String nama, String email){
        return usersRepo.findByNamaAndEmail(nama,email);
    }

}
