package com.example.demo.repo;

import com.example.demo.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepo extends JpaRepository<Users,Integer> {
    List<Users> findAll();
    @Query(value = "SELECT * FROM t_users where username=?1 and email=?1", nativeQuery = true)
    Users findByNamaAndEmail(String username, String email);
}
