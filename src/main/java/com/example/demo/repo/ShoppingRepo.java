package com.example.demo.repo;

import com.example.demo.domain.Shopping;
import com.example.demo.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShoppingRepo extends JpaRepository<Shopping, Integer> {

    Shopping findAllById(Integer idShopping);

    @Query(value = "SELECT * FROM t_shopping", nativeQuery = true)
    List<Shopping> findAllShopping();


}
