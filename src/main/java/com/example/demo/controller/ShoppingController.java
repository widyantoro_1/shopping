package com.example.demo.controller;

import com.example.demo.domain.Shopping;
import com.example.demo.domain.Users;
import com.example.demo.model.login;
import com.example.demo.model.response;
import com.example.demo.service.ShoppingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;

@RestController()
@RequestMapping("/users")
public class ShoppingController {
    @Autowired
    ShoppingService shoppingService;

    @GetMapping("/AllUsers")
    public ResponseEntity getAllUsers(){
        LinkedHashMap res = new LinkedHashMap();
        List<Users> data = shoppingService.findAllUsers();
        res.put("data", data);
        return ResponseEntity.ok().body(res);
    }
    @GetMapping("/AllShopping")
    public ResponseEntity getAllShopping(){
        LinkedHashMap res = new LinkedHashMap();
        List<Shopping> data = shoppingService.ShoppingAll();
        res.put("data", data);
        return ResponseEntity.ok().body(res);
    }
    @GetMapping("Shopping/{idShopping}")
    public ResponseEntity getbyIdShopping(@PathVariable Integer idShopping){
        LinkedHashMap res = new LinkedHashMap();
        Shopping data = shoppingService.findByIdShopping(idShopping);
        res.put("data", data);
        return ResponseEntity.ok().body(res);
    }
    @PostMapping("/signup")
    public ResponseEntity insert(@RequestBody Users users){

        LinkedHashMap res = new LinkedHashMap();
        response respon= new response();
        try {
            shoppingService.save(users);
            respon.setNama(users.getNama());
            respon.setEmail(users.getEmail());
            res.put("message", "data berhasil masuk");
            res.put("Data", users);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("message", "Gagal");
            return ResponseEntity.badRequest().body(res);
        }
    }
    @PostMapping("/signin")
    public ResponseEntity signin(@RequestBody login users){

        LinkedHashMap res = new LinkedHashMap();
        try {
            String username = users.getUsername();
            String email = users.getEmail();
            Users data = shoppingService.findUsersByNamaAndEmail(username,email);
            res.put("message", "data users tersedia");
            res.put("Data", data);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("message", "Gagal");
            return ResponseEntity.badRequest().body(res);
        }
    }

    @PutMapping("/update/shopping/{Id}")
    public ResponseEntity update(@RequestBody Shopping shopping,@PathVariable Integer Id){

        LinkedHashMap res = new LinkedHashMap();
        try {
            shoppingService.update(shopping, Id);
            res.put("message", "data berhasil diupdate");
            res.put("Data", shopping);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("message", "Gagal");
            return ResponseEntity.badRequest().body(res);
        }
    }
    @PostMapping("/shopping")
    public ResponseEntity insertshopping(@RequestBody Shopping shopping){

        LinkedHashMap res = new LinkedHashMap();
        try {
            shoppingService.saveShopping(shopping);
            res.put("message", "data berhasil masuk");
            res.put("Data", shopping);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("message", "Gagal");
            return ResponseEntity.badRequest().body(res);
        }
    }

    @DeleteMapping("delete/shopping/{id}")
    public ResponseEntity delete(@PathVariable("id") Integer id){

        LinkedHashMap res = new LinkedHashMap();
        try {

            shoppingService.delete(id);
            res.put("message", "data berhasil delete");
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("message", "Gagal");
            return ResponseEntity.badRequest().body(res);
        }
    }

}
